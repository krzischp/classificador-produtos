#!/bin/bash

gunicorn -w 4 -b 127.0.0.1:5000 app:app &
# nginx -g 'daemon off;' &
# Para substituir dinamicamente (na hora de subir o contêiner) 
# o valor de $PORT no default pelo valor sorteado pelo Heroku e que será informado ao contêiner como uma variável de ambiente
sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/sites-available/default && nginx -g 'daemon off;' &

wait -n

exit $?
