[Access project on GitLab](https://gitlab.com/krzischp/classificador-produtos/-/tree/main)
# classificador produtos

# Buiding image and running on Docker
```bash
docker build -t krzischp/classificador-produtos .
docker run -d -p 8080:9361 --env PORT=9361 --rm --name classificador-produtos-container krzischp/classificador-produtos
```
Stop and remove the container:
```bash
docker stop classificador-produtos-container
docker rm classificador-produtos-container
```

Job configuration in `.gitlab-ci.yml`

# Runner
Grant execution privileges for your runner to execute `Docker` commands:  
- `/etc/gitlab-runner/config.toml` if you're on Linux
- `~/.gitlab-runner/config.toml` if you're on MacOS


```bash
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "minilinux"
  url = "https://gitlab.com/"
  token = "HpyD5Wzv6YPZgCrEKgGo"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
-    image = "python:3.9.6-slim"
+    image = "docker:19.03.12"
-    privileged = false
+    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
-    volumes = ["/cache"]
+    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```
